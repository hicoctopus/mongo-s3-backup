# Mongo S3 Backup


*~~Inspired by~~ Copied from an old project: https://github.com/mondora/mongo-s3-backup*

----

Image: `registry.gitlab.com/hicoctopus/mongo-s3-backup`

----


This container runs a cron job which:

- dumps a mongo database using `mongodump`
- archives the dump and uploads it to Amazon S3
- Contains a command to restore a specific backup from the S3.

## Environment variables 

You can configure the execution of the above steps by setting the following
environment variables:

* `MONGO_HOST` Hostname of the container with the database (must be in the same network as this backup container).
* `MONGO_PORT` Port for Mongodb.
* `MONGO_DB`[optional, all databases if empty] Defines the database to backup.
* `AWS_ACCESS_KEY_ID` AWS credential
* `AWS_SECRET_ACCESS_KEY` AWS credential
* `S3_BUCKET` AWS S3 bucket name to store the backups
* `BACKUP_FILENAME_PREFIX` [optional, defaults to `mongo_backup`] Backup file name prefix.
* `BACKUP_FILENAME_DATE_FORMAT` [optional, defaults to `%Y%m%d`] datetime format in backup filename.
* `CRON_SCHEDULE` [optional, defaults to `0 1 * * *`] Defines when a backup should be created

### AWS credentials

Create a new IAM user in AWS and give it the following policy:

```json
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "s3:ListBucket",
                "s3:DeleteObject",
                "s3:GetBucketLocation"
            ],
            "Resource": [
                "arn:aws:s3:::bucketName",
                "arn:aws:s3:::bucketName/*"
            ]
        }
    ]
}
```
Replace `bucketName` with the actual name of your bucket.

## Manual operation

It is possible to force a backup or restore by invoking a command on the docker host.

### Manual backup

Run

```bash
docker exec name_of_backup_container /script/backup.sh
```

Where `name_of_backup_container` is the name or id of the (running) container.

### Restore

Run

```bash
docker exec name_of_backup_container /script/restore.sh mongo_backup_202002010.tgz
```
Where `name_of_backup_container` is the name or id of the (running) container and `mongo_backup_202002010.tgz` is the name of the backup existing in the S3 bucket.
