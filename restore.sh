#!/bin/bash
set -e

# Utility function
get_date () {
    date +[%Y-%m-%d\ %H:%M:%S]
}

if [ -z "$1" ]
then
    echo "No backup filename specified. Please provide the name of the file on S3 to restore:"
    echo "   restore.sh some_backup_file.tgz"
else
    BACKUP_FILENAME="$1"
fi

echo $BACKUP_FILENAME

# Script

echo "$(get_date) Mongo backup restore started"
[ -d /restore ] || mkdir /restore
[ -d /restore/data ] || mkdir /restore/data

echo "$(get_date) [Step 1/3] Retreive backup from S3"
/usr/local/bin/aws s3 cp s3://$S3_BUCKET/$BACKUP_FILENAME /restore/$BACKUP_FILENAME

echo "$(get_date) [Step 2/3] Unpack the backup"
tar -zxvf /restore/$BACKUP_FILENAME -C /restore/data


echo "$(get_date) [Step 3/3] Restore data to mongodb"
if [ -z "$MONGO_DB" ]
then
    RESTORECMD="mongorestore -h $MONGO_HOST --port $MONGO_PORT /restore/data/dump"
else
    RESTORECMD="mongorestore -h $MONGO_HOST --port $MONGO_PORT -d $MONGO_DB /restore/data/dump/$MONGO_DB"
fi
eval $RESTORECMD
rm -rf /restore/*

echo "$(get_date) Mongo backup restored successfully"

